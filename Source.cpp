#include<iostream>

using namespace std;

void printTable(char[]);
int winCheck(char[]);

int main() {
	char table[] = {'-','-','-' ,'-' ,'-' ,'-' ,'-' ,'-' ,'-'};
	int turn = 1;
	int select = 0;
	int win = 0;

	printTable(table);

	while(win == 0) {
		cout <<"Player "<< turn << " turn!!  where do you want to add ?? : ";
		cin >> select;
		if (turn == 1 && table[select - 1] == '-') {
			table[select - 1] = 'X';
			turn = 2;
		}
		if (turn == 2 && table[select - 1] == '-') {
			table[select - 1] = 'O';
			turn = 1;
		}
		
		printTable(table);
		win = winCheck(table);
		
	}
	

	if (win == 3) {
		cout << "DRAW!!!!!" <<endl;
	}
	if (win == 1) {
		cout << "Player 1 WIN!!!!!"<<endl;
	}
	if (win == 2) {
		cout << "Player 2 WIN!!!!!"<<endl;

	}

	//system("pause");
	return 0;
}

void printTable(char table[]) {
	for (int i = 0; i < 9; i++) {
		if (i % 3 == 0) {
			cout << endl;
			cout << "[" << table[i] << "] ";
		}
		else {
			cout << "[" << table[i] << "] ";
		}
		
	}

}

int winCheck(char table[]) {
	int win = 0;
	if (table[0] == 'X' &&table[1] == 'X' &&table[2] == 'X') {
		win = 1;
	}
	 if (table[3] == 'X' &&table[4] == 'X' &&table[5] == 'X') {
		win = 1;
	}
	 if (table[6] == 'X' &&table[7] == 'X' &&table[8] == 'X') {
		win = 1;
	}
	 if (table[0] == 'X' &&table[3] == 'X' &&table[6] == 'X') {
		win = 1;
	}
	 if (table[1] == 'X' &&table[4] == 'X' &&table[7] == 'X') {
		win = 1;
	}
	 if (table[2] == 'X' &&table[5] == 'X' &&table[8] == 'X') {
		win = 1;
	}
	 if (table[0] == 'X' &&table[4] == 'X' &&table[8] == 'X') {
		win = 1;
	}
	 if (table[2] == 'X' &&table[4] == 'X' &&table[6] == 'X') {
		win = 1;
	}

	 if (table[0] == 'O' &&table[1] == 'O' &&table[2] == 'O') {
		win = 2;
	}
	 if (table[3] == 'O' &&table[4] == 'O' &&table[5] == 'O') {
		win = 2;
	}
	 if (table[6] == 'O' &&table[7] == 'O' &&table[8] == 'O') {
		win = 2;
	}
	 if (table[0] == 'O' &&table[3] == 'O' &&table[6] == 'O') {
		win = 2;
	}
	 if (table[1] == 'O' &&table[4] == 'O' &&table[7] == 'O') {
		win = 2;
	}
	 if (table[2] == 'O' &&table[5] == 'O' &&table[8] == 'O') {
		win = 2;
	}
	 if (table[0] == 'O' &&table[4] == 'O' &&table[8] == 'O') {
		 win = 2;
	 }
	 if (table[2] == 'O' &&table[4] == 'O' &&table[6] == 'O') {
		win = 2;
	}
	  if (win == 0 &&table[0] != '-'&& table[1] != '-'&& table[2] != '-'&& table[3] != '-'&& table[4] != '-'&& table[5] != '-'&& table[6] != '-'&& table[7] != '-'&& table[8] != '-') {
		  win = 3;
	 }

	return win;
}